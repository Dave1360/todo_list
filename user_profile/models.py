from django.db import models
from django.contrib.auth.models import User
from phone_field import PhoneField
from django.db.models.signals import post_save
from django.dispatch import receiver


class User_Profile(models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        primary_key=True,
    )
    mobileNumber = PhoneField(
        blank=False, unique=False, help_text="Contact phone number"
    )
    RANK = (
        ("Gold", ("Gold membership")),
        ("Silver", ("Silver membership")),
        ("Basic", ("Basic membership")),
    )
    rank = models.CharField(max_length=32, choices=RANK, default="Basic")
    kappa = models.CharField(max_length=1)

    def __str__(self):
        return f"{self.user} - {self.mobileNumber}"