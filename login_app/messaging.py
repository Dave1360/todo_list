from django.core.mail import send_mail


def email_message(message_dict):
    contents = f"""
    Hi, thank you for trying to reset your password
    Click this link to reset your password: {message_dict['reset_link']}
    """

    send_mail(
        "Password Reset Link",
        contents,
        "dfraenkel3@gmail.com",
        [message_dict["email_receiver"]],
        fail_silently=False,
    )