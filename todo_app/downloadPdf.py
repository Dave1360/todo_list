from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, FileResponse
from django.urls import reverse
from .models import Todo
from django.contrib.auth.decorators import login_required
from reportlab.pdfgen import canvas
from reportlab.lib.units import inch, cm
import io
import django_rq


def generate_pdf():
    buffer = io.BytesIO()
    p = canvas.Canvas(buffer)

    p.drawString(50, 10.5 * inch, "Hello world")

    p.showPage()
    p.save()
    buffer.seek(0)
    print(buffer)
    return buffer