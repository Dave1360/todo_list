from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, FileResponse
from django.urls import reverse
from .models import Todo
from django.contrib.auth.decorators import login_required
from reportlab.pdfgen import canvas
from reportlab.lib.units import inch, cm
import io
import django_rq
from .downloadPdf import generate_pdf

# Create your views here.


@login_required
def index(request):
    if request.method == "POST":
        text = request.POST["text"]
        Todo.create(request.user, text)

    todos = Todo.objects.filter(status=False).filter(user=request.user)
    context = {"todos": todos}

    return render(request, "todo_app/index.html", context)


@login_required
def completed_todos(request):
    todos = Todo.objects.filter(status=True).filter(user=request.user)
    context = {"todos": todos}
    return render(request, "todo_app/completed_todos.html", context)


@login_required
def change_status(request):
    pk = request.POST["pk"]
    todo = get_object_or_404(Todo, pk=pk)
    todo.toggleStatus()
    return HttpResponseRedirect(request.META["HTTP_REFERER"])


@login_required
def delete_todo(request):
    pk = request.POST["pk"]
    todo = get_object_or_404(Todo, pk=pk)
    todo.delete()
    return HttpResponseRedirect(request.META["HTTP_REFERER"])


def details(request):
    pk = request.GET.get("pk")
    print(pk)
    todo = get_object_or_404(Todo, pk=pk)
    context = {"todo": todo}
    return render(request, "todo_app/details.html", context)


def pdf(request):
    # todos = Todo.objects.filter(user=request.user)
    buffer = django_rq.enqueue(generate_pdf)
    return FileResponse(buffer, as_attachment=True, filename="hello.pdf")
